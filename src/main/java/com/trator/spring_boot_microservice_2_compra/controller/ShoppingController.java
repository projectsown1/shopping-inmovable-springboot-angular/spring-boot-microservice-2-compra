package com.trator.spring_boot_microservice_2_compra.controller;

import com.trator.spring_boot_microservice_2_compra.model.Shopping;
import com.trator.spring_boot_microservice_2_compra.service.ShoppingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/shopping")
public class ShoppingController {

    private final ShoppingService shoppingService;

    public ShoppingController(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @PostMapping
    public ResponseEntity<?> saveShopping(@RequestBody Shopping shopping){
        return new ResponseEntity<>(
                shoppingService.saveShopping(shopping),
                HttpStatus.CREATED
        );
    }

    @GetMapping("{userId}")
    public ResponseEntity<?> getAllShoppingOfUser(@PathVariable Long userId){
        return ResponseEntity.ok(shoppingService.findAllShoppingOfUser(userId));
    }
}
