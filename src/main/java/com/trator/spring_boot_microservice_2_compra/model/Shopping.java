package com.trator.spring_boot_microservice_2_compra.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Table(name="shopping")
@Entity
public class Shopping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="user_id",nullable = false)
    private Long userId;

    @Column(name="inmovable_id",nullable = false)
    private Long inmovableId;

    @Column(name="title", nullable = false)
    private String title;

    @Column(name="price",nullable = false)
    private Double price;

    @Column(name = "created_at",nullable = false)
    private LocalDateTime created_at;


}
