package com.trator.spring_boot_microservice_2_compra.repository;

import com.trator.spring_boot_microservice_2_compra.model.Shopping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShoppingRepository extends JpaRepository<Shopping,Long> {

    List<Shopping> findAllByUserId(Long userId);
}
