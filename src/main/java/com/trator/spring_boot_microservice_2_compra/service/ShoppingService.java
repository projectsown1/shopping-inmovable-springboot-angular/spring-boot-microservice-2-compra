package com.trator.spring_boot_microservice_2_compra.service;

import com.trator.spring_boot_microservice_2_compra.model.Shopping;

import java.util.List;

public interface ShoppingService {
    Shopping saveShopping(Shopping shopping);

    List<Shopping> findAllShoppingOfUser(Long userId);
}
