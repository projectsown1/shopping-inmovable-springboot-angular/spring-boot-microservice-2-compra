package com.trator.spring_boot_microservice_2_compra.service;

import com.trator.spring_boot_microservice_2_compra.model.Shopping;
import com.trator.spring_boot_microservice_2_compra.repository.ShoppingRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingServiceImpl implements ShoppingService{

    private final ShoppingRepository shoppingRepository;


    public ShoppingServiceImpl(ShoppingRepository shoppingRepository) {
        this.shoppingRepository = shoppingRepository;
    }

    @Override
    public Shopping saveShopping(Shopping shopping){
        shopping.setCreated_at(LocalDateTime.now());
        return shoppingRepository.save(shopping);
    }

    @Override
    public List<Shopping> findAllShoppingOfUser(Long userId){
        return shoppingRepository.findAllByUserId(userId);
    }
}
